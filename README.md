# client_ftp

The Client_ftp program allows you  to download and upload files to an
ftp server

![project](./img/menu.png)

FTP server login panel

![project2](./img/sesja.png)

FTP client management panel

## Features

1) Login Panel
2) Saving login data
3) Reading login data
4) Creating files and folders on ftp server and on the local computer
5) Deleting files on ftp server and on the local computer
6) Renaming files on ftp server and on the local computer
7) Downloading and uploading files
8) Creating zip files

## Table of Content

* [Running and installation](#running)
* [Technologies](#technologies)
* [Authors](#authors)

## Running and installation

**Maven must be installed before running the project**

### Running

```maven
mvn clean compile package javafx:run
```

## Technologies

Project is created with:

* javafx 13.0
* maven 3.8.1
* junit 5.6.2
* openjdk 13
* commons-net 3.6

## Authors

*****
**goodbit22** --> <https://gitlab.com/users/goodbit22>
*****
**hagaven**   --> <https://gitlab.com/hagaven>
*****
