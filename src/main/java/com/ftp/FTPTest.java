package com.ftp;

import com.graficzny.Pass;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * w tej klasie znajduje sie wszystkie testy jednostkowe
 */
class FTPTest {
    private FTP session;
    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        Pass daneLog = new Pass("www.mkwk018.cba.pl", "reszta@goodbit.c0.pl", "Gr13_baza11", "ftp", 21);
        this.session=new FTP(daneLog);
    }

    /**
     * Test sprawdza czy funkcja poprawnia zwróci błąd związany z próbą pobrania
     * pliku z pustą nazwą
     */
    @Test
    void pobieranieFtpShouldThrowException() {
        try {
            session.pobieranieFtp("");
            fail();
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }

    /**
     * Test sprawdza czy funkcja poprawnia zwróci błąd związany z próbą przekazania
     * pliku z pustą nazwą na serwer FTP
     */
    @Test
    void dodawanie_ftpShouldThrowException() {
        try{
            session.dodawanie_ftp("");
            fail();
        }catch (Exception e){
            e.getMessage();
            e.printStackTrace();
        }
    }

}