package com.ftp;

import javafx.scene.control.TextArea;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * w tej klasie znajduje sie wszystkie operacja  dla sesji lokalnej uzytkownika
 */
public class DodatkoweFunkcjeKlienta {
    /**
     * @return Zwraca listę plików w obecnym katalogu
     */
    public  List<String> wyswietl_lokalnie(){
        File obecny_katalog = new File(".");
        File[] files = obecny_katalog.listFiles();
        List<String> list_files = new LinkedList<>();
        if (files != null) {
            for (File file : files) {
                if (file.isFile()){
                    String details = file.getName();
                    double bajt = file.length();
                    if (bajt < 1024) {
                        details += "\t\t\t" + bajt + "B";
                    } else {
                        double kilobajt = bajt / 1024.0;
                        if (kilobajt < 1024) {
                            String kilo = String.format("%.2f",kilobajt);
                            details += "\t\t\t" + kilo + "Kib";
                        } else {
                            double megabytes = kilobajt / 1024.0;
                            if (megabytes < 1024) {
                                String mega = String.format("%.2f",megabytes);
                                details += "\t\t\t" + mega + "MiB";
                            } else {
                                double gigabytes = (megabytes / 1024);
                                String giga = String.format("%.2f",gigabytes);
                                details += "\t\t\t" + giga + "GiB";
                            }
                        }
                    }
                    list_files.add(details);
                }else{
                    double bajt;
                    bajt = getFolderSize(file);
                    String details = file.getName();
                    if (bajt < 1024) {
                        details += "\t\t\t" + bajt + "B";
                    } else {
                        double kilobajt = bajt / 1024.0;
                        if (kilobajt < 1024) {
                            String kilo = String.format("%.2f",kilobajt);
                            details += "\t\t\t" + kilo + "Kib";
                        } else {
                            double megabytes = kilobajt / 1024.0;
                            if (megabytes < 1024) {
                                String mega = String.format("%.2f",megabytes);
                                details += "\t\t\t" + mega + "MiB";
                            } else {
                                double gigabytes = (megabytes / 1024);
                                String giga = String.format("%.2f",gigabytes);
                                details += "\t\t\t" + giga + "GiB";
                            }
                        }
                    }
                    list_files.add(details);
                }
            }
        }
        return list_files;
    }

    private long getFolderSize(File folder) {
        long length = 0;
        File[] files = folder.listFiles();
        if (files == null || files.length == 0){
            return length;
        }
        for (File file : files) {
            if (file.isFile()) {
                length += file.length();
            } else {
                length += getFolderSize(file);
            }
        }
        return length;
    }

    /**
     * @param name_file nazwa pliku do archiwozacji
     * @param text_comunicate pole tekstowe do wyświetlania komunikatów
     *                       związanych z wynikowym działaniem funkcji
     * @throws IOException wyrzuca błędy związane z brakiem dostępu do pliku
     */
    public void mozliwosc_archiwizacji_plikow(String name_file, TextArea text_comunicate) throws IOException {
        File filetozip = new File(name_file);
        if(!filetozip.exists()){
            text_comunicate.setText("nie udalo sie utworzyc pliku zip");
            text_comunicate.setStyle("-fx-text-fill: red;");
        }else{
            String nazwa_zipa = name_file+".zip";
            FileOutputStream fos = new FileOutputStream(nazwa_zipa);
            ZipOutputStream zipOut = new ZipOutputStream(fos);
            FileInputStream fis = new FileInputStream(filetozip);
            ZipEntry zipEntry = new ZipEntry(filetozip.getName());
            zipOut.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int legth;
            while ((legth = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, legth);
            }
            zipOut.close();
            fis.close();
            fos.close();
        }
    }

    /**
     * @return zwraca ścieżkę do obecnego katalogu użytkownika
     */
    public String wyswietlanie_sciezek_lokalny() {
        return System.getProperty("user.dir");
    }

    /**
     * @param directory_name nazwa katalogu na komputerze użytkownika do którego ma przejść program
     * @param text_comunicate pole tekstowe do wyświetlania komunikatów
     *      *                       związanych z wynikowym działaniem funkcji
     * @param operation określa czy fukcja ma przejść do katalogu czy cofnąć się
     */
    public void zmiana_katalogu_lokalnego(String directory_name,TextArea text_comunicate,String operation) {
        if(operation.equals("przejdz")){
            File katalog = new File(directory_name);
            if(katalog.isDirectory() && katalog.exists()){
                text_comunicate.clear();
                text_comunicate.appendText("Stara sciezka = " + System.getProperty("user.dir"));
                text_comunicate.appendText("\n");
                String nazwa_katalogu = System.getProperty("user.dir")+"/"+directory_name;
                System.setProperty("user.dir", nazwa_katalogu);
                text_comunicate.appendText("Nowa sciezka =" + System.getProperty("user.dir"));
                text_comunicate.setStyle("-fx-text-fill: green;");
                //wyswietl_lokalnie();
            }else{
                text_comunicate.setText("nie udalo sie przejsc do katalogu");
                text_comunicate.setStyle("-fx-text-fill: red;");
            }
        }else {
            String nazwa_katalogu = System.getProperty("user.dir");
            char[] c = nazwa_katalogu.toCharArray();
            StringBuilder test = new StringBuilder("/");
            int indeks = 0;
            for(int i=0; i<c.length; i++){
                if(c[i] == '/'){
                    indeks = i;
                }
            }
            for(int j=1; j<indeks; j++){
                test.append(c[j]);
            }
            System.setProperty("user.dir", String.valueOf(test));
            text_comunicate.setText("Nowa sciezka = " + test);
            text_comunicate.setStyle("-fx-text-fill: green;");
        }
    }

    /**
     * @param name_file nazwa tworzonego pliku
     * @param text_comunicate pole tekstowe do wyświetlania komunikatów
     *                       związanych z wynikowym działaniem funkcji
     * @throws IOException wyrzucany błąd związany jest z błędem dotyczącym tworzenia pliku o nazwie określonej przez parametr
     */
    public void tworzenie_plikow_lokalnych(String name_file, TextArea text_comunicate) throws IOException {
        File file = new File(name_file);
        boolean stan = file.createNewFile();
        if (stan) {
            text_comunicate.setText("utworzono plik");
            text_comunicate.setStyle("-fx-text-fill: green;");
        } else{
            text_comunicate.setText(" nie utworzono pliku");
            text_comunicate.setStyle("-fx-text-fill: red;");
        }
    }

    /**
     * @param name_catalog nazwa tworzonego katalogu
     * @param text_comunicate pole tekstowe do wyświetlania komunikatów
     *                     związanych z wynikowym działaniem funkcji
     */
    public void tworzenie_katalogow_lokalnych(String name_catalog,TextArea text_comunicate) {
        File file = new File(name_catalog);
        boolean stan = file.mkdir();
        if (stan) {
            text_comunicate.setText("utworzono katalog");
            text_comunicate.setStyle("-fx-text-fill: green;");
        }
        else{
            text_comunicate.setText(" nie utworzono katalogu");
            text_comunicate.setStyle("-fx-text-fill: red;");
        }
    }

    /**
     * @param name_file nazwa usuwanego pliku
     * @param text_comunicate pole tekstowe do wyświetlania komunikatów
     *                       związanych z wynikowym działaniem funkcji
     */
    public void usuwanie_plikow_lokalnych(String name_file,TextArea text_comunicate) {
        File obecny_katalog = new File(".");
        File[] files = obecny_katalog.listFiles();
        File file2 = null;
        assert files != null;
        for (File file : files) {
            String details = file.getName();
            if (details.equals(name_file)) {
                file2 = file;
            }
        }
        assert file2 != null;
        boolean stan = file2.delete();
        if (stan) {
            text_comunicate.setText("Pomyslnie usunieto plik");
            text_comunicate.setStyle("-fx-text-fill: green;");
        } else {
            text_comunicate.setText("nie udalo sie usunac pliku");
            text_comunicate.setStyle("-fx-text-fill: red;");
        }
    }

    /**
     * @param directory_name nazwa usuwanego katalogu
     * @param text_comunicate pole tekstowe do wyświetlania komunikatów
     *                       związanych z wynikowym działaniem funkcji
     */
    public void usuwanie_katalogow_lokalnych(String directory_name,TextArea text_comunicate){
        File directory = new File(directory_name);
        if(!directory.exists()){
            text_comunicate.setText("Nie ma takiego katalogu");
            text_comunicate.setStyle("-fx-text-fill: red;");
        }
        else{
             two_function_remove_catalog(directory,text_comunicate);
            }
    }

    /**
     * @param directory usuwany katalog
     * @param text_comunicate pole tekstowe do wyświetlania komunikatów
     *      *                       związanych z wynikowym działaniem funkcji
     */
    public void two_function_remove_catalog (File directory,TextArea text_comunicate ) {
        if(directory.isDirectory()){
            if (directory.list().length == 0) {
                directory.delete();
                text_comunicate.setText("Usunieto  katalog");
                text_comunicate.setStyle("-fx-text-fill: green;");
            } else {
                String[] files = directory.list();
                for (String temp : files) {
                    File fileDelete = new File(directory, temp);
                    two_function_remove_catalog(fileDelete, text_comunicate);
                }
                if (directory.list().length == 0) {
                    directory.delete();
                    text_comunicate.setText("Usunieto  katalog");
                    text_comunicate.setStyle("-fx-text-fill: green;");
                }else{
                    directory.delete();
                    text_comunicate.setText("Usunieto  katalog");
                    text_comunicate.setStyle("-fx-text-fill: green;");
                }
             }
        }else{
            text_comunicate.setText("podany katalog jest plikiem");
            text_comunicate.setStyle("-fx-text-fill: red;");
        }
    }

    /**
     * @param old_name_file nazwa pliku którego nazwa jest zmieniana
     * @param new_name_file nazwa na którą zmianiana jest nazwa pliku
     * @param text_comunicate pole tekstowe do wyświetlania komunikatów
     *                       związanych z wynikowym działaniem funkcji
     */
    public void zmiana_nazwy_plikow_lokalnych(String old_name_file, String new_name_file,TextArea text_comunicate ) {
        if(!(old_name_file.isEmpty()) &&  !(new_name_file.isEmpty())  ){
            File obecny_katalog = new File(".");
            File[] files = obecny_katalog.listFiles();
            File file2 = null;
            assert files != null;
            for (File file : files) {
                String details = file.getName();
                if (details.equals(old_name_file)) {
                    file2 = file;
                }else{
                    text_comunicate.setText("Nie ma takiego pliku ");
                    text_comunicate.setStyle("-fx-text-fill: red;");
                }
            }
            File file12 = new File(new_name_file);
            assert file2 != null;
            if(file2.renameTo(file12)){
                text_comunicate.setText("udalo sie zmienic nazwe pliku");
                text_comunicate.setStyle("-fx-text-fill: green;");
            }else{
                text_comunicate.setText("Nie udalo sie zmienic nazwe pliku");
                text_comunicate.setStyle("-fx-text-fill: red;");
            }
        }else{
            text_comunicate.setText("Nie wypelniles obu pol");
            text_comunicate.setStyle("-fx-text-fill: red;");
        }
    }
}
