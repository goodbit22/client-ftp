package com.ftp;


import com.graficzny.Pass;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;


public class FTP extends Thread {
    Pass dane_logowania;
    public FTPClient klient;

    /**
     * @param dane_logowania dane używane do ustanowienia połączenia z serwerem FTP
     */
    public FTP(Pass dane_logowania){
        this.dane_logowania=dane_logowania;
        klient=new FTPClient();
    }

    /**
     * Funckja służąca do nawiązania połączenia z serwerem FTP
     */
    public void polacz() {
        try {
            klient.connect(dane_logowania.getHost());
           int reply = klient.getReplyCode();
            if(reply!=220){
                throw new Exception("wyjatek w polaczeniu z serwerem ftp "+reply);
            }
            try{
                boolean stan_log=klient.login(dane_logowania.getLogin(),dane_logowania.getPassword());
                if(!stan_log){
                    throw new Exception("niewłaściwy login lub hasło");
                }
            }catch (Exception e)
            {
                klient.disconnect();
                e.getMessage();
                e.printStackTrace();
                FTP.windowPopUp(e.getMessage(),"Statement Authentication Error");
            }
        }catch (Exception e){
            try {
                klient.disconnect();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
            FTP.windowPopUp(e.getMessage(),"Statement Authentication Error");
        }
        klient.enterLocalPassiveMode();
    }

    /**
     * Funkcja służąca do kończenia połączenia z serwerem FTP
     * @throws Exception wyrzucane błędy związane są ze stanem połączenia z serwerem
     */
    public void rozlacz() throws Exception{
        if(klient.isConnected()){
            klient.logout();
            klient.disconnect();
        }
    }

    /**
     * @param path_ftp pole tekstowe służące do wyświetlenia obecnej ścieżki w katalogu serwera ftp
     * @throws Exception wyrzucane błędy związane są ze stanem połączenia z serwerem
     */
    public void display_current_path_ftp(TextField path_ftp) throws Exception {
        polacz();
        String sciezka_ftp = klient.printWorkingDirectory();
        System.out.println("dubnkcja dispaly current path " + sciezka_ftp);
        path_ftp.setEditable(true);
        path_ftp.setText(sciezka_ftp);
        path_ftp.setEditable(false);

        rozlacz();
    }

    /**
     * @param listview_ftp lista służąca do wyświetlania plików
     *                     znajdujących się na serwerze FTP
     * @throws Exception wyrzucane błędy związane są ze stanem połączenia z serwerem
     */
    public void displays_ftp_files(ListView<String> listview_ftp) throws Exception {
        ObservableList<String> listview_zapis = FXCollections.observableArrayList();
        List<String> list_files_ftp = wyswietl_ftp();
        listview_zapis.addAll(list_files_ftp);
        listview_ftp.setItems(listview_zapis);
    }

    /**
     * @param sciezka nazwa katalogu tworzonego na serwerze FTP
     * @param text_comunicate pole tekstowe do wyświetlania komunikatów
     *                       związanych z wynikowym działaniem funkcji
     * @throws Exception wyrzucane błędy związane są ze stanem połączenia z serwerem
     */
    public void makeDirectory(String sciezka,TextArea text_comunicate) throws Exception {
        polacz();
        boolean created = klient.makeDirectory(sciezka);
        if(created){
            text_comunicate.setText("Pomyslnie utworzono katalog w ftp ");
            text_comunicate.setStyle("-fx-text-fill: green;");
        }else{
            text_comunicate.setText("nie udalo sie utworzyc katalogu w ftp ");
            text_comunicate.setStyle("-fx-text-fill: red;");
        }
        rozlacz();
    }

    /**
     * @param file nazwa pliku który ma zostać usunięty z serwera FTP
     * @param typ określa czy usuwany jest plik czy katalog
     * @param text_comunicate pole tekstowe do wyświetlania komunikatów
     *                      związanych z wynikowym działaniem funkcji
     * @throws Exception wyrzucane błędy związane są ze stanem połączenia z serwerem
     */
    public void usuwanie_ftp(String file,String typ,TextArea text_comunicate) throws Exception {
        polacz();
        try{
            if(typ.equals("file")){
                boolean deleted = klient.deleteFile(file);
                if(deleted){
                    text_comunicate.setText("the file was deleted successfully");
                    windowPopUp("Pomyślnie usunięto plik","Usuwanie pliku");
                    text_comunicate.setStyle("-fx-text-fill: green;");
                }
                else{
                    text_comunicate.setText("nie udalo sie usunac pliku na ftp");
                    windowPopUp("Nie udało się usunąć pliku","Usuwanie pliku");
                    text_comunicate.setStyle("-fx-text-fill: red;");
                }
            }
            else{
                boolean usuwanie_katalogu = klient.removeDirectory(file);
                if(usuwanie_katalogu){
                    text_comunicate.setText("the catalog was deleted successfully");
                    text_comunicate.setStyle("-fx-text-fill: green;");
                }else{
                    text_comunicate.setText("nie udalo sie usunac katalogu na ftp");
                    text_comunicate.setStyle("-fx-text-fill: red;");
                }
            }
        } catch(IOException ex){
            text_comunicate.setText("Oh no, there was an error: " + ex.getMessage());
            text_comunicate.setStyle("-fx-text-fill: red;");
        }
        rozlacz();
    }

    /**
     * @param wiadomosc wiadomość jaka ma zostać wyświetlona w oknie komunikatu
     * @param title tytuł okna komunikatu
     */
    public static void windowPopUp(String wiadomosc,String title)  {
        Label statement = new Label(wiadomosc);
        Button exit = new Button("ok");
        exit.setMaxWidth(100);
        VBox vbox = new VBox();
        VBox.setMargin(statement,new Insets(10));
        VBox.setMargin(exit,new Insets(1));
        vbox.getChildren().addAll(statement,exit);
        Scene scene = new Scene(vbox);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setWidth(405);
        stage.setTitle(title);
        stage.setResizable(false);
        stage.show();
        exit.setOnAction(actionEvent -> stage.close());
    }

    /**
     * @param old_name_file nazwa pliku którego nazwa ma być zmieniona
     * @param new_name_file nowa nazwa pliku
     * @throws Exception wyrzucane błędy związane są ze stanem połączenia z serwerem
     */
    public void zmiana_nazwy_pliku_ftp(String old_name_file,String new_name_file) throws Exception {
        polacz();
        if(old_name_file!=null&&new_name_file!=null){
            boolean stan = klient.rename(old_name_file,new_name_file);
            if(stan){
                System.out.println("zmiana nazwy sie powiodla");
            }
        }
        else{
            windowPopUp("Oba pola muszą być wypełnione","Niepoprawna nazwa pliku");
        }
        rozlacz();
    }

    public void zmiana_katalogu(String nazwa,TextArea text_comunicate) throws Exception {
        polacz();
        boolean success = klient.changeWorkingDirectory(klient.printWorkingDirectory()  + "/" + nazwa);
        if (success) {
            text_comunicate.setText("Successfully changed working directory.");
            text_comunicate.setStyle("-fx-text-fill: green;");
            //System.out.println(klient.printWorkingDirectory());
        } else {
            text_comunicate.setText("Failed to change working directory. See server's reply.");
            text_comunicate.setStyle("-fx-text-fill: red;");
        }
        rozlacz();
    }
    private void showServerReply() {
        try {
            String reply=""+klient.getReply();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param nazwa nazwa pliku który ma zostać pobrany
     * @throws Exception wyrzucane błędy związane są ze stanem połączenia z serwerem
     */
    public void pobieranieFtp(String nazwa) throws Exception {
        polacz();
        klient.setFileType(FTPClient.BINARY_FILE_TYPE);
        OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(nazwa));
        boolean success = klient.retrieveFile(nazwa,outputStream);
        outputStream.flush();
        outputStream.close();
        if(success){
            windowPopUp("Pomyślnie pobrano plik","Pobieranie");
        }
        rozlacz();
    }

    /**
     * @param filename nazwa pliku który ma zostać przekazany na serwer FTP
     * @throws Exception wyrzucane błędy związane są ze stanem połączenia z serwerem
     */
    public void dodawanie_ftp(String filename) throws Exception {
        InputStream inputStream = new FileInputStream(filename);
        polacz();
        klient.setFileType(FTPClient.BINARY_FILE_TYPE);
        klient.setFileTransferMode(FTPClient.BINARY_FILE_TYPE);
        boolean done = klient.storeFile(filename,inputStream);
        if(done){
            windowPopUp("Pomyślnie dodano plik","Dodawanie pliku");
        }
        else {
            windowPopUp("Nie udało się dodać pliku","Dodawanie plku");
        }
        rozlacz();
        inputStream.close();
    }

    /**
     * @return Zwraca listę plików znajdujących się na serwerze FTP
     * @throws Exception wyrzucane błędy związane są ze stanem połączenia z serwerem
     */
    public List<String> wyswietl_ftp() throws Exception {
        polacz();
        FTPFile[] files = klient.listFiles();
        DateFormat dateFormat = new SimpleDateFormat("yyyy=MM-dd HH:mm:ss");
        List<String> list_files_ftp = new LinkedList<>();
        for(FTPFile file : files){
            String details = file.getName();
            details +=  "\t\t" + file.getSize();
            details += "\t" + file.getGroup();
            details += " \t" + dateFormat.format(file.getTimestamp().getTime());
            list_files_ftp.add(details);
        }
        rozlacz();
        return list_files_ftp;
    }
}
