package com.graficzny;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * w tej klasie znajduje deinicja okna uruchamianego przy starcie programu
 */
public class MainWindow extends Application {
    /**
     * Metoda która tworzy okno glowne programu i laduje plik fxml
     * @param primarystage  przekuzeje glowne scene
     */
    @Override
    public void start(Stage primarystage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainView.fxml"));
        Scene scene = new Scene(root);
        primarystage.setScene(scene);
        primarystage.getIcons().add(new Image("ikony/klient.png"));
        primarystage.setOnCloseRequest((e)-> MainWindowController.zamknij_okno());
        primarystage.setTitle("Klient usług");
        primarystage.setResizable(false);
        primarystage.show();
    }
    /**
     * Metoda która uruchamia program
     */
    public static void main(String[] args) {
        launch();
    }

}