package com.graficzny;
/**
 * w tej klasie definiowany obiekt ktory jest opisuje dane do logowania
 */
public class Pass {
    private String host;
    private String login;
    private String password;
    private int port;
    private String protokol;

    /**
     * @param host adres serwera FTP
     * @param login nazwa użytkownika
     * @param password hasło użytkownia
     * @param protokol protokół połączenia
     * @param port port połączenia
     */
    public Pass(String host, String login, String password, String protokol, int port){
        this.host=host;
        this.login=login;
        this.password=password;
        this.port=port;
        this.protokol=protokol;
    }
    public Pass(){
        this.host="localhost";
        this.login="admin";
        this.password="admin";
        this.port=21;
        this.protokol="ftp";
    }

    public String getHost() {
        return host;
    }
    public String getLogin(){
        return login;
    }
    public String getPassword(){
        return password;
    }
    public String getProtokol(){
        return  protokol;
    }
    public int getPort(){
        return port;
    }

    @Override
    public String toString() {
        return  host + ";" + login + ";" + ";" + protokol + ";" + port + ";" + "\n";
    }
}
