package com.graficzny;

import com.ftp.FTP;
import com.ftp_graphics.FtpWindow;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;
/**
 * w tej klasie znajduje controler dla  okna zdefiniowanego  MainWindow
 */
public class MainWindowController implements Initializable{
    @FXML private Button przycisk_zamknij;
    @FXML private Button przycisk_logowania;
    @FXML private Button przycisk_zapisz;
    @FXML private CheckBox przycisk_anonimowy;
    @FXML private TextField pole_host;
    @FXML private TextField pole_login;
    @FXML private PasswordField pole_haslo;
    @FXML private Spinner<Integer> numer_pr;
    SpinnerValueFactory<Integer> licznik  = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,500,1);
    @FXML private ComboBox<String> rodzaj_pr;
    ObservableList<String> list = FXCollections.observableArrayList("ftp","sftp","telnet","ssh");
    @FXML private  ListView<Pass> lista_zapisanych_danych;
    ObservableList<Pass> list_zapis = FXCollections.observableArrayList();
    ContextMenu list_option = new ContextMenu();

    /**
     * Metoda sluzaca do wywolanie inicjalizowego controlera MainWindowController  dla pliku fxml MainView.fxml ,contorler zawiera konfiguracje obiektow stworzony przez plik fxml
     * @param url  Lokalizacja używana do rozpoznawania ścieżek względnych dla obiektu głównego lub wartość null, jeśli lokalizacja nie jest znana.
     * @param  resourceBundle używane do zlokalizowania obiektu głównego lub zerowego, jeśli obiekt główny nie został zlokalizowany
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ImageView zap_zdj = new ImageView("ikony/save.png");
        zap_zdj.setFitHeight(25);zap_zdj.setFitWidth(20);
        przycisk_zapisz.setGraphic(zap_zdj);
        ImageView log_zdj = new ImageView("ikony/logon.png");
        log_zdj.setFitHeight(25);log_zdj.setFitWidth(20);
        przycisk_logowania.setGraphic(log_zdj);
        ImageView exit_zdj = new ImageView("ikony/exit.png");
        exit_zdj.setFitHeight(25);exit_zdj.setFitWidth(20);
        przycisk_zamknij.setGraphic(exit_zdj);
        ImageView rekord_zdj = new ImageView("ikony/rekord.png");
        rekord_zdj.setFitHeight(10);rekord_zdj.setFitWidth(10);
        numer_pr.setValueFactory(licznik);
        rodzaj_pr.setItems(list);
        rodzaj_pr.setValue("ftp");
        licznik.setValue(21);
        MenuItem menuItem1 = new MenuItem("Usun wpis");
        menuItem1.setOnAction(t -> {
            int w = lista_zapisanych_danych.getSelectionModel().getSelectedIndex();
            if(!list_zapis.isEmpty()){
                list_zapis.remove(w);
            }
            lista_zapisanych_danych.setItems(list_zapis);
        });
        MenuItem menuItem2 = new MenuItem("Zmien wpis");
        menuItem2.setOnAction(actionEvent -> {
            ObservableList<Pass> name;
            int w = lista_zapisanych_danych.getSelectionModel().getSelectedIndex();
            name = lista_zapisanych_danych.getSelectionModel().getSelectedItems();
            for (Pass name1 : name) {
                System.out.println(name1);
                System.out.println(w);
            }
        });
        MenuItem menuItem3 = new MenuItem("Wczytaj wpis");
        menuItem3.setOnAction(actionEvent -> {
            ObservableList<Pass>  name;
            name = lista_zapisanych_danych.getSelectionModel().getSelectedItems();
            Pass importowane = new Pass();
            for(Pass name1: name){
                importowane=name1;
            }
            pole_host.setText(importowane.getHost());
            pole_login.setText(importowane.getLogin());
            pole_haslo.setText(importowane.getPassword());
            rodzaj_pr.setValue(importowane.getProtokol());
            Integer port=importowane.getPort();
            numer_pr.getValueFactory().setValue(port);
        });
        MenuItem menuItem4 = new MenuItem("Importuj wpisy");
        menuItem4.setOnAction(actionEvent -> {
        String wczytywanyWpis;
        String[] wczytywane;
        try{
            BufferedReader bufor= Files.newBufferedReader(Paths.get("src/main/resources/program_files/save1.txt"));
            while ((wczytywanyWpis=bufor.readLine())!=null){
                wczytywane=wczytywanyWpis.split(";");
                Pass tmp=new Pass(wczytywane[0],wczytywane[1],wczytywane[2],wczytywane[3],Integer.parseInt(wczytywane[4]));
                list_zapis.add(tmp);
                lista_zapisanych_danych.setItems(list_zapis);
            }
        }catch (IOException e){
            FTP.windowPopUp("Brak zapisanych wpisow","Nie znaleziono pliku");
        }
        });
        list_option.getItems().add(menuItem1);
        list_option.getItems().add(menuItem2);
        list_option.getItems().add(menuItem3);
        list_option.getItems().add(menuItem4);
        lista_zapisanych_danych.setContextMenu(list_option);
    }

    /**
     * Funkcja służąca do włączenia anonimowego logowania na serwer FTP
     */
    public void session_anonymous(){
        if(!przycisk_anonimowy.isSelected()){
            pole_haslo.setDisable(false);
            pole_login.setDisable(false);
            pole_login.setText("anonymous");
            pole_haslo.setText("");
        } else{
            pole_haslo.setDisable(true);
            pole_login.setDisable(true);
        }
    }
    /**
     * Funkcja służąca do zamknięcia programu
     */
    public void zamknij_program(){
        Platform.exit();
        System.exit(0);
    }

    /**
     * Funkcja służąca do zapisu danych logowania do pliku oraz do tablicy wpisów
     * ostatnio używanych danych logowania
     */
    public void zapisz_danych(){
        String host =  pole_host.getText();
        String login =  pole_login.getText();
        String password =  pole_haslo.getText();
        String protokol = rodzaj_pr.getValue();
        int port = 21;
        try {
            port = numer_pr.getValue();
        }catch(NumberFormatException e){
            e.printStackTrace();
        }
        Pass pass=new Pass(host,login,password,protokol,port);
        String calosc = host + ";" + login + ";" + password + ";" + protokol + ";" + port + ";" + "\n" ;
        list_zapis.add(pass);
        lista_zapisanych_danych.setItems(list_zapis);
        FileWriter zapis=null;
        try{
            zapis=new FileWriter("src/main/resources/program_files/save1.txt",true);
            zapis.write(calosc);
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try{
                if(zapis!=null){
                    zapis.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Funkcja służy do wywołania okna obsługi sesji połączenia z danym serwerem
     */
    public void logowanie() {
        String host =  pole_host.getText();
        String login =  pole_login.getText();
        String password =  pole_haslo.getText();
        String protokol = rodzaj_pr.getValue();
        int port = numer_pr.getValue();
        Pass daneLog=new Pass(host,login,password,protokol,port);
        new FtpWindow(daneLog);
    }

    /**
     * Funkcja służąca do zamknięcia okna programu
     */
    public static void zamknij_okno(){
        Platform.exit();
        System.exit(0);
    }
}
