package com.ftp_graphics;

import com.graficzny.Pass;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
/**
 * w tej klasie znajduje implementacja okna wyswietlajace sie  po zalogowanie sie do serwera frp
 */
public class FtpWindow extends Application {
    FXMLLoader fxmlLoader;
    FtpWindowController controller;
    Pass daneLog;

    /**
     * @param daneLog dane służące do nawiązania połączenia z serwerem FTP
     */
    public FtpWindow(Pass daneLog)  {
        fxmlLoader=new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("fxml/FtpView.fxml"));
        Parent root= null;
        try {
            root = fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        fxmlLoader.setRoot(root);
        controller=fxmlLoader.getController();
        this.daneLog=daneLog;
        Stage primary_stage=new Stage();
        controller.setDaneLog(daneLog);
        Scene scene = null;
        if (root != null) {
            scene = new Scene(root);
        }
        primary_stage.setScene(scene);
        primary_stage.getIcons().add(new Image("ikony/ikony_ftp/logo_ftp.jpg"));
        primary_stage.setTitle("Sesja FTP");
        primary_stage.setResizable(false);
        primary_stage.show();
    }
    /**
     * Metoda która
     */
    @Override
    public void start(Stage stage) {


    }
}
