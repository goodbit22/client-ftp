package com.ftp_graphics;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import com.ftp.DodatkoweFunkcjeKlienta;
import com.ftp.FTP;
import com.graficzny.Pass;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 * w tej klasie znajduje controler dla  okna zdefiniowanego FtpWindow
 */
public class FtpWindowController implements Initializable , Runnable{
    @FXML private MenuButton dodaj_folder;
    @FXML private MenuButton dodaj_plik;
    @FXML private Button zip_plik;
    @FXML private MenuButton usun_plik;
    @FXML private MenuButton usuwanie_folder;
    @FXML private MenuButton zmiana_nazwy_plik ;
    @FXML private MenuButton zmiana_katalogu;
    @FXML private Button upload_file ;
    @FXML private TextField path_local;
    @FXML private TextField path_ftp;
    @FXML public ListView<String> listview_local;
    @FXML public ListView<String>  listview_ftp;
    @FXML public TextArea text_comunicate;
    @FXML public MenuItem option_language;
    @FXML public MenuItem option_authors;
    @FXML public MenuItem option_check_for_update;
    ContextMenu listview_option_ftp;
    ContextMenu listview_option_local;
    ObservableList<String> listview_zapis = FXCollections.observableArrayList();
    FTP session;
    Pass daneLog;

    public FtpWindowController() {
        listview_option_ftp = new ContextMenu();
        listview_option_local = new ContextMenu();
    }

    /**
     * Metoda sluzy do tworzenie okno który wyswietla sie przy wybranie odpowiedniej opcji
     * @param operation okresla jaka operacje zostanie wykonana w tym oknie
     * @param  tekst ustawia tytul okna zaleznosc od wybranej operacji
     */
    public void insert_window(String operation,String tekst) {
        Label etykieta = new Label(tekst);
        TextField field = new TextField();
        Button przycisk = new Button("Wykonaj");
        HBox hbox = new HBox(etykieta,field,przycisk);
        Scene scene = new Scene(hbox);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Wprowadzanie wartosc");
        stage.setResizable(false);
        switch (operation) {
            case "add_folder_local": {
                DodatkoweFunkcjeKlienta funkcjeklienta = new DodatkoweFunkcjeKlienta();
                przycisk.setOnAction(actionEvent -> {
                    funkcjeklienta.tworzenie_katalogow_lokalnych(field.getText(),text_comunicate) ;
                    stage.close();
                    displays_local_files();
                });
                break;
            }
            case "add_folder_ftp": {
                przycisk.setOnAction(actionEvent ->
                {
                    try {
                        session.makeDirectory(field.getText(),text_comunicate);
                        stage.close();
                        session.displays_ftp_files(listview_ftp);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                break;
            }
            case "add_file_local": {
                DodatkoweFunkcjeKlienta funkcjeklienta = new DodatkoweFunkcjeKlienta();
                przycisk.setOnAction(actionEvent -> {
                    try {
                        funkcjeklienta.tworzenie_plikow_lokalnych(field.getText(),text_comunicate);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    stage.close();
                    displays_local_files();
                });
                break;
            }
            case "remove_file_local": {
                DodatkoweFunkcjeKlienta funkcjeklienta = new DodatkoweFunkcjeKlienta();
                przycisk.setOnAction(
                        actionEvent ->{
                            funkcjeklienta.usuwanie_plikow_lokalnych(field.getText(),text_comunicate);
                            stage.close();
                            displays_local_files();
                });
                break;
            }
            case "remove_file_ftp": {
                przycisk.setOnAction(actionEvent -> {
                            try {
                                session.usuwanie_ftp(field.getText(), "file",text_comunicate);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            stage.close();
                            try {
                                session.displays_ftp_files(listview_ftp);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                break;
            }
            case "create_file_zip": {
                DodatkoweFunkcjeKlienta funkcjeklienta = new DodatkoweFunkcjeKlienta();
                przycisk.setOnAction(  actionEvent -> {
                    try {
                        funkcjeklienta.mozliwosc_archiwizacji_plikow(field.getText(), text_comunicate);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    stage.close();
                    displays_local_files();
                });
                break;
            }
            case "remove_directory_local": {
                DodatkoweFunkcjeKlienta funkcjeklienta = new DodatkoweFunkcjeKlienta();
                przycisk.setOnAction(
                        actionEvent -> {
                            funkcjeklienta.usuwanie_katalogow_lokalnych(field.getText(), text_comunicate);
                            stage.close();
                            displays_local_files();
                        }
                );
                break;
            }
            case "remove_directory_ftp":{
                przycisk.setOnAction(actionEvent -> {
                    try {
                        session.usuwanie_ftp(field.getText(), "catalog",text_comunicate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    stage.close();
                    try {
                        session.displays_ftp_files(listview_ftp);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                break;
            }
            case "upload_file_ftp":{
                przycisk.setOnAction(actionEvent->{
                    try{
                        session.dodawanie_ftp(field.getText());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    stage.close();
                    displays_local_files();
                    try {
                        session.displays_ftp_files(listview_ftp);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
            default: {
                text_comunicate.setText("nie wybrano zadnej operacji");
                break;
            }
        }
        stage.setScene(scene);
        stage.setTitle("Wprowadzanie wartosc");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }
    /**
     * Metoda sluzy do tworzenie okno który wyswietla sie  przy zmianie nazwy pliku
     * @param operation okresla gdzie zostanie wykonana operacja zmiany pliku czy na ftp czy na sesji lokalnej
     */
    public void insert_window_two(String operation) {
        Label etykieta = new Label("stara nazwa pliku:  ");
        Label etykieta2 = new Label("nowa nazwa pliku: ");
        TextField field = new TextField();
        TextField field2 = new TextField();
        Button przycisk = new Button("Wykonaj");
        przycisk.setMaxWidth(300);
        HBox hbox = new HBox(etykieta,field);
        HBox hbox2 = new HBox(etykieta2,field2);
        VBox vbox = new VBox(hbox,hbox2,przycisk);
        vbox.setFillWidth(true);
        Scene scene = new Scene(vbox);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Zmiana nazwy");
        stage.setResizable(false);
        if(operation.equals("rename_file_local")){
            DodatkoweFunkcjeKlienta funkcjeklienta = new DodatkoweFunkcjeKlienta();
            przycisk.setOnAction(actionEvent -> {
                funkcjeklienta.zmiana_nazwy_plikow_lokalnych(field.getText(), field2.getText(),text_comunicate);
                stage.close();
                displays_local_files();
            });
        }
        else{
            przycisk.setOnAction(actionEvent -> {
                try {
                    session.zmiana_nazwy_pliku_ftp(field.getText(),
                            field2.getText());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                stage.close();
                try {
                    session.displays_ftp_files(listview_ftp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    /**
     * Metoda sluzy do tworzenie okno który wyswietla sie  przy zmianie katalogu
     * @param operation okresla gdzie zostanie wykonana operacja zmiany katalogu czy na ftp czy na sesji lokalnej
     * @param tekst ustawia tytul okna zaleznosc od wybranej operacji
     */
    public void change_directory_window(String operation,String tekst)  {
        Label etykieta = new Label("Podaj do jakiego katalogu chcesz przejsc");
        TextField field = new TextField();
        Button przycisk = new Button("Przejdz");
        przycisk.setMaxWidth(270);
        Button przycisk2 = new Button("Powrot");
        przycisk2.setMaxWidth(270);
        VBox vbox = new VBox(etykieta,field,przycisk,przycisk2);
        vbox.setFillWidth(true);
        Scene scene = new Scene(vbox);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle(tekst);
        stage.setResizable(false);
        if(operation.equals("katalog_local")){
            DodatkoweFunkcjeKlienta funkcjeklienta = new DodatkoweFunkcjeKlienta();
            przycisk.setOnAction(actionEvent -> {
                funkcjeklienta.zmiana_katalogu_lokalnego(field.getText(),text_comunicate,"przejdz");
                stage.close();
                display_current_path_local();
            });
            przycisk2.setOnAction(actionEvent -> {
                funkcjeklienta.zmiana_katalogu_lokalnego("",text_comunicate,"back");
                stage.close();
                display_current_path_local();
            });
        }
        else{
            przycisk.setOnAction(actionEvent -> Platform.runLater(() -> {
                try {
                    session.zmiana_katalogu(field.getText(), text_comunicate);
                    stage.close();
                    session.displays_ftp_files(listview_ftp);
                    session.display_current_path_ftp(path_ftp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }));
            przycisk2.setOnAction(actionEvent -> Platform.runLater(() -> {
                try {
                    session.zmiana_katalogu(field.getText(), text_comunicate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                stage.close();
                try {
                    session.display_current_path_ftp(path_ftp);
                    session.displays_ftp_files(listview_ftp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }));
        }
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }
    public void language_func() throws FileNotFoundException {
        RadioButton radioButton_pl  = new RadioButton("Polski");
        RadioButton radioButton_eng = new RadioButton("English");
        File file = new File("src/main/resources/program_files/language.txt");
        Scanner read_file = new Scanner(file);
        String data = read_file.nextLine();
        if (data.equals("pl"))
            radioButton_pl.setSelected(true);
        else{
            radioButton_eng.setSelected(true);
        }
        ToggleGroup tg = new ToggleGroup();
        radioButton_eng.setToggleGroup(tg);
        radioButton_pl.setToggleGroup(tg);
        Button button_accept = new Button("Apply");
        Label status = new Label();
        button_accept.setPrefSize(260.0,26);
        button_accept.setOnAction(actionEvent -> {
            if(radioButton_pl.isSelected()){
                FileWriter fileWriter = null;
                try {
                    fileWriter = new FileWriter("src/main/resources/program_files/language.txt");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    assert fileWriter != null;
                    fileWriter.write("pl");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                FileWriter fileWriter = null;
                try {
                    fileWriter = new FileWriter("src/main/resources/program_files/language.txt");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    assert fileWriter != null;
                    fileWriter.write("eng");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            status.setText("ok");
        });
        Image image = new Image("/ikony/menu_bar_icons/flag_pl.png");
        Image image1 = new Image("/ikony/menu_bar_icons/flag_eng.png");
        ImageView flags_pl =new ImageView(image);
        flags_pl.setFitWidth(200);
        flags_pl.setFitHeight(150);
        ImageView flags_eng = new ImageView(image1);
        flags_eng.setFitWidth(190);
        flags_eng.setFitHeight(150);
        AnchorPane anchorPane = new AnchorPane(radioButton_pl, radioButton_eng, button_accept, flags_pl, flags_eng,status);
        AnchorPane.setLeftAnchor(radioButton_pl,75.0);
        AnchorPane.setBottomAnchor(radioButton_pl,100.0);
        AnchorPane.setRightAnchor(radioButton_eng,75.0);
        AnchorPane.setBottomAnchor(radioButton_eng,100.0);
        AnchorPane.setBottomAnchor(button_accept,50.0);
        AnchorPane.setRightAnchor(button_accept,75.0);
        AnchorPane.setTopAnchor(flags_pl,20.0);
        AnchorPane.setLeftAnchor(flags_pl,10.0);
        AnchorPane.setTopAnchor(flags_eng,20.0);
        AnchorPane.setRightAnchor(flags_eng,10.0);
        AnchorPane.setBottomAnchor(status,30.0);
        AnchorPane.setLeftAnchor(status,200.0);
        Scene scene = new Scene(anchorPane, 400, 290);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Language");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }
    public  void authors_func(){
        BackgroundImage backgroundImage = new BackgroundImage( new Image( getClass().getResource("/ikony/menu_bar_icons/authors.png").toExternalForm()),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                new BackgroundSize(1.0, 1.0, true, true, false, false));
        Background background = new Background(backgroundImage);
        Button button_author1 =new Button();
        button_author1.setPrefWidth(150);
        button_author1.setPrefHeight(100);
        button_author1.setBackground(background);
        button_author1.setOnAction(actionEvent -> new Thread(() -> {
            try {
                Desktop.getDesktop().browse(new URI("https://gitlab.com/goodbit22"));
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
        }).start());
        BackgroundImage backgroundImage2 = new BackgroundImage( new Image( getClass().getResource("/ikony/menu_bar_icons/authors1.png").toExternalForm()),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                new BackgroundSize(1.0, 1.0, true, true, false, false));
        Background background2 = new Background(backgroundImage2);
        Button button_author2 =new Button();
        button_author2.setPrefWidth(150);
        button_author2.setPrefHeight(100);
        button_author2.setBackground(background2);
        button_author2.setOnAction(actionEvent -> new Thread(() -> {
            try {
                Desktop.getDesktop().browse(new URI("https://gitlab.com/hagaven"));
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
        }).start());
        HBox hBox = new HBox(button_author1,button_author2);
        Scene scene = new Scene(hBox,300,100);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Authors");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    public void check_for_update_func(){
        Button button_ok = new Button("ok");
        ProgressBar progressBar = new ProgressBar();
        progressBar.setPrefWidth(200);
        Label label = new Label("Sprawdzanie aktualizacji:");
        AnchorPane anchorPane_main = new AnchorPane(button_ok,progressBar,label);
        AnchorPane.setTopAnchor(progressBar,15.0);
        AnchorPane.setBottomAnchor(button_ok,5.0);
        AnchorPane.setLeftAnchor(button_ok,160.0);
        Scene scene = new Scene(anchorPane_main,200,70);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Update");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }
    /**
     * Metoda sluzy do ustawiania tla na przyciski
     *
     */
    public void setting_background_buttons(){
        BackgroundImage backgroundImage = new BackgroundImage( new Image( getClass().getResource("/ikony/ikony_ftp/katalog_add.png").toExternalForm()),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(0.9, 1.0, true, true, false, false));
        BackgroundImage backgroundImage2 = new BackgroundImage( new Image( getClass().getResource("/ikony/ikony_ftp/file_add.png").toExternalForm()),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(1.0, 1.0, true, true, false, false));
        BackgroundImage backgroundImage3 = new BackgroundImage( new Image( getClass().getResource("/ikony/ikony_ftp/zip.png").toExternalForm()),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(1.0, 1.0, true, true, false, false));
        BackgroundImage backgroundImage4 = new BackgroundImage( new Image( getClass().getResource("/ikony/ikony_ftp/file_sub.png").toExternalForm()),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(1.0, 1.0, true, true, false, false));
        BackgroundImage backgroundImage5 = new BackgroundImage( new Image( getClass().getResource("/ikony/ikony_ftp/catalog_delete.png").toExternalForm()),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(0.9, 1.0, true, true, false, false));
        BackgroundImage backgroundImage6 = new BackgroundImage( new Image( getClass().getResource("/ikony/ikony_ftp/rename_file.png").toExternalForm()),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(1.0, 1.0, true, true, false, false));
        BackgroundImage backgroundImage7 = new BackgroundImage( new Image( getClass().getResource("/ikony/ikony_ftp/change_folder.png").toExternalForm()),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(1.0, 0.9, true, true, false, false));
        BackgroundImage backgroundImage8 = new BackgroundImage( new Image( getClass().getResource("/ikony/ikony_ftp/upload_ftp.png").toExternalForm()),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(1.0, 1.0, true, true, false, false));
        Background background = new Background(backgroundImage);
        Background background2 = new Background(backgroundImage2);
        Background background3 = new Background(backgroundImage3);
        Background background4 = new Background(backgroundImage4);
        Background background5 = new Background(backgroundImage5);
        Background background6 = new Background(backgroundImage6);
        Background background7 = new Background(backgroundImage7);
        Background background8 = new Background(backgroundImage8);
        dodaj_folder.setBackground(background);
        dodaj_plik.setBackground(background2);
        usun_plik.setBackground(background4);
        zip_plik.setBackground(background3);
        usuwanie_folder.setBackground(background5);
        zmiana_nazwy_plik.setBackground(background6);
        zmiana_katalogu.setBackground(background7);
        upload_file.setBackground(background8);
    }
    /**
     * Metoda sluzaca do wywolanie inicjalizowanego controlera FtpWindowController dla pliku fxml FtpView.fxml ,contorler zawiera konfiguracje obiektow stworzony przez plik fxml
     * @param url  Lokalizacja używana do rozpoznawania ścieżek względnych dla obiektu głównego lub wartość null, jeśli lokalizacja nie jest znana.
     * @param  resourceBundle używane do zlokalizowania obiektu głównego lub zerowego, jeśli obiekt główny nie został zlokalizowany
     */
    @Override
    public void  initialize(URL url, ResourceBundle resourceBundle) {
            Platform.runLater(()-> {
                this.session=new FTP(daneLog);
                setting_background_buttons();
                MenuItem menuItem1 = new MenuItem("Zmien nazwe");
                menuItem1.setOnAction(actionEvent -> rename_file_local());
                MenuItem menuItem2 = new MenuItem("Wrzuc na ftp");
                menuItem2.setOnAction(actionEvent -> {
                    ObservableList<String> nazwa=listview_local.getSelectionModel().getSelectedItems();
                    String[] pliki=new String[10];
                    for(String dodawane:nazwa){
                        pliki=dodawane.split("\t");
                    }
                    try{
                        session.dodawanie_ftp(pliki[0]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    displays_local_files();
                    try {
                        session.displays_ftp_files(listview_ftp);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                MenuItem menuItem3 = new MenuItem("Usun plik");
                menuItem3.setOnAction(actionEvent -> {
                    ObservableList<String> nazwa=listview_local.getSelectionModel().getSelectedItems();
                    String[] pliki=new String[10];
                    for(String usuwane:nazwa){
                        pliki=usuwane.split("\t");
                    }
                    DodatkoweFunkcjeKlienta funkcjeklienta = new DodatkoweFunkcjeKlienta();
                    try{
                        funkcjeklienta.usuwanie_plikow_lokalnych(pliki[0],text_comunicate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    displays_local_files();
                    try {
                        session.displays_ftp_files(listview_ftp);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                listview_option_local.getItems().add(menuItem1);
                listview_option_local.getItems().add(menuItem2);
                listview_option_local.getItems().add(menuItem3);
                listview_local.setContextMenu(listview_option_local);
                MenuItem menuItem1_ftp = new MenuItem("Zmien nazwe");
                menuItem1_ftp.setOnAction(actionEvent -> rename_file_ftp());
                MenuItem menuItem2_ftp = new MenuItem("Pobierz na lokalny komputer");
                menuItem2_ftp.setOnAction(actionEvent -> {
                    ObservableList<String> nazwa=listview_ftp.getSelectionModel().getSelectedItems();
                    String[] pliki=new String[10];
                    for(String pobierane:nazwa){
                        pliki=pobierane.split("\t");
                    }
                    try {
                        session.pobieranieFtp(pliki[0]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        session.displays_ftp_files(listview_ftp);
                        displays_local_files();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                MenuItem menuItem3_ftp = new MenuItem("Usun plik");
                menuItem3_ftp.setOnAction(actionEvent -> {
                    ObservableList<String> nazwa=listview_ftp.getSelectionModel().getSelectedItems();
                    String[] pliki=new String[10];
                    for(String usuwane:nazwa){
                        pliki=usuwane.split("\t");
                    }
                    try {
                        session.usuwanie_ftp(pliki[0], "file",text_comunicate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        session.displays_ftp_files(listview_ftp);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                listview_option_ftp.getItems().add(menuItem1_ftp);
                listview_option_ftp.getItems().add(menuItem2_ftp);
                listview_option_ftp.getItems().add(menuItem3_ftp);
                listview_ftp.setContextMenu(listview_option_ftp);
                display_current_path_local();
                try {
                    session.display_current_path_ftp(path_ftp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                displays_local_files();
                try {
                    session.displays_ftp_files(listview_ftp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
    }

    /**
     * Funkcja wyświetlająca ścieżkę do obecnego katalogu użytkownika
     */
    public void display_current_path_local(){
        DodatkoweFunkcjeKlienta funkcja_sciezka = new DodatkoweFunkcjeKlienta();
        String sciezka = funkcja_sciezka.wyswietlanie_sciezek_lokalny();
        path_local.setEditable(false);
        path_local.setText(sciezka);
    }

    /**
     * Funkcja wyświetlająca pliki w obecnym katalogu użytkownika
     */
    public void displays_local_files(){
        listview_local.getItems().clear();
        DodatkoweFunkcjeKlienta  dodatkoweFunkcjeKlienta  = new DodatkoweFunkcjeKlienta();
        List<String>list_files =  dodatkoweFunkcjeKlienta.wyswietl_lokalnie();
        listview_zapis.addAll(list_files);
        listview_local.setItems(listview_zapis);
    }

    /**
     * Funkcja wywołująca okno tworzenia nowego katalogu w bieżącym katalogu użytkownika
     */
    public void add_folder_local() {
        insert_window("add_folder_local","nazwa nowego folderu");
    }

    /**
     * Funkcja wywołująca okno tworzenia nowego katalogu w bieżącym katalogu serwera FTP
     */
    public void add_folder_ftp() {
        insert_window("add_folder_ftp","nazwa nowego folderu");
    }

    /**
     * Funkcja wywołująca okno tworzenia nowego pliku w bieżącym katalogu użytkownika
     */
    public void add_file_local() {
        insert_window("add_file_local","nazwa nowego pliku");
    }

    /**
     * Funkcja wywołująca okno usuwania pliku w bieżącym katalogu użytkownika
     */
    public void remove_file_local() {
        insert_window("remove_file_local","nazwa folderu do usuwania");
    }

    /**
     * Funkcja wywołująca okno usuwania pliku w bieżącym katalogu serwera FTP
     */
    public void remove_file_ftp() {
        insert_window("remove_file_ftp","nazwa folderu do usuwania");
    }

    /**
     * Funkcja wywołująca okno tworzenia pliku zip
     */
    public void create_file_zip() {
        insert_window("create_file_zip","nazwa pliku do  zipowania");
    }

    /**
     * Funkcja wywołująca okno usuwania katalogu z bieżącego katalogu użytkownika
     */
    public void remove_directory_local () {
        insert_window("remove_directory_local","nazwa katalogu do usununiecia");
    }

    /**
     * Funkcja wywołująca okno usuwania katalogu z bieżącego katalogu serwera FTp
     */
    public void remove_directory_ftp() {
        insert_window("remove_directory_ftp","nazwa katalogu do usununiecia");
    }

    /**
     * Funkcja wywołuje okno zmiany nazwy pliku z bieżącego katalogu użytkownika
     */
    public void rename_file_local () {
        insert_window_two("rename_file_local");
    }

    /**
     * Funkcja wywołuje okno zmiany nazwy pliku z bieżącego katalogu serwera FTP
     */
    public  void rename_file_ftp() {
        insert_window_two("rename_file_ftp");
    }

    /**
     * Funkcja wywołuje okno zmiany bieżącego katalogu użytkownika
     */
    public void change_directory_local () {
        change_directory_window("katalog_local","katalog local");
    }

    /**
     * Funckja wywołuje okno zmiany bieżącego katalogu serwera FTP
     */
    public void change_directory_ftp () {
       change_directory_window("katalog_ftp","katalog ftp");
    }

    /**
     * Funkcja wywołuje okno dodawnia pliku z bieżącego katalogu na serwer FTP
     */
    public void upload_file_ftp() {
        insert_window("upload_file_ftp","plik których chcesz wysłać na serwer");
    }

    /**
     * @param daneLog dane logowania służące do nawiązywania połączenia z serwerem
     */
    public void setDaneLog(Pass daneLog) {
        this.daneLog=daneLog;
    }

    @Override
    public void run() {

    }
}
